import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";

import {UserProvider} from "./context/UserContext"
import {AdminProvider} from "./context/AdminContext"
import { CartProvider } from "./context/CartContext"

import { initialState, reducer } from "./reducer/UserReducer";
import { initialState2, reducer2 } from "./reducer/AdminReducer";
import { CartReducer } from "./reducer/CartReducer";
import { useReducer } from "react";

//css
import './App.css';

// Components
import AppNavbar from './components/AppNavbar';
import Footer from './components/Footer';

//Pages
import Home from "./pages/Home";
import Products from "./pages/Products";
import Register from './pages/Register';
import Login from "./pages/Login";
import ErrorPage from "./pages/ErrorPage";
import Logout from "./pages/Logout";
import AdminView from "./pages/AdminView";
import Cart from "./pages/Cart";
import OrderHistory from "./pages/OrderHistory";

function App() {

  const [ state, dispatch ] = useReducer(reducer, initialState)
  const [ state2, dispatch2 ] = useReducer(reducer2, initialState2)
  const [ stateCart, dispatchCart ] = useReducer(CartReducer, [])

  return(
    <UserProvider value ={{state, dispatch}}>
      <AdminProvider value= {{state2, dispatch2}}>
        <CartProvider value= {{stateCart, dispatchCart}}>
          <BrowserRouter>
            <AppNavbar />
            <Routes>
              <Route path="/" element={<Home />}/>
              <Route path="/products" element={<Products />}/>
              <Route path="/cart" element={<Cart />}/>
              <Route path="/checkout" element={<OrderHistory />}/>
              <Route path="/adminView" element={<AdminView />}/>
              <Route path="/register" element={<Register />}/>
              <Route path="/login" element={<Login />}/>
              <Route path="/logout" element={<Logout />}/>
              <Route path="*" element={<ErrorPage />}/>
            </Routes>
            <Footer />
          </BrowserRouter>
          </CartProvider>
      </AdminProvider>  
    </UserProvider>
  )
}

export default App;
