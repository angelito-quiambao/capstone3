import { Fragment, useState } from "react"
import {Card, Col, Modal, Button} from "react-bootstrap"
import prodIcon from "./../images/prod_icon.jpg"
import ProductModal from "./ProductModal"

export default function ProductCart({productProp}){
    const {productName, description, price, _id} = productProp
    // console.log(productProp)

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return(
        <Fragment>
            <Col xs={12} lg={4} className="mx-auto mb-3">
                <Card>
                <Card.Img variant="top" className="img-fluid px-3 pt-3" src={prodIcon} />
                <Card.Body className="p-3">
                    <Card.Title className="font-weight-Bold">{productName}</Card.Title>
                    <Card.Subtitle className="font-weight-Bold">Description:</Card.Subtitle>
                    <Card.Text>
                        {description}
                    </Card.Text>

                    <Card.Subtitle className="font-weight-Bold mt-2">Price: <span className="font-weight-Bold mb-4">&#8369; {price}.00</span> </Card.Subtitle>

                    <Card.Link className="btn btn-outline-dark btn-block mt-3" onClick={() => handleShow()}>View Product</Card.Link>
                </Card.Body>
                </Card>
            </Col>

            <Modal show={show} onHide={handleClose} size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
            <Modal.Header closeButton className="bg-dark">
            <Modal.Title className="text-light">{productName}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <ProductModal prodProps={_id}/>
            </Modal.Body>
            </Modal>
        </Fragment>
    )
}