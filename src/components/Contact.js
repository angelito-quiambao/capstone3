import { Carousel, Image } from "react-bootstrap";
import img1 from "./../images/img1.jpg"
import img2 from "./../images/img2.jpg"
import img3 from "./../images/img3.jpg"

export default function Contact(){

    return(
        <section className="row px-2 px-lg-5 my-5 secHeight">
				<div className="col-12 align-self-end text-dark text-center">
					<h1 className="display-4 my-3 my-lg-5 py-lg-2">Message Us Now!</h1>
				</div>

				<div className="col-12 col-lg-6">
					<form className="p-3">

					  <div className="form-group">
					    <label htmlFor="full_name" className="text-dark">Name</label>
					    <input type="text" className="form-control" id="full_name" placeholder="Your Name" required />
					  </div>

					  <div className="form-group">
					    <label htmlFor="email" className="text-dark">Email address</label>
					    <input type="email" className="form-control" id="email" placeholder="name@example.com" required />
					  </div>

					  <div className="form-group">
					    <label htmlFor="contact_number" className="text-dark">Contact Number</label>
					    <input type="number" className="form-control" id="contact_number" placeholder="+63 900 000 0000" min="0" required />
					  </div>

					  <div className="form-group">
					    <label htmlFor="mesSub" className="text-dark">Message Subject</label>
					    <select className="form-control" id="mesSub required">
					    	<option>Product/Service Query</option>
					    	<option>Book a Meeting</option>
					    </select>
					  </div>

					  <div className="form-group">
					    <label htmlFor="message" className="text-dark">Message</label>
					    <textarea className="form-control" id="message" rows="5" required></textarea>
					  </div>
					  
					  <button type="submit" className="btn btn-dark d-block mx-auto my-3">Send Email</button>

					  <div className="modal fade" id="thankyou" tabIndex="-1" aria-labelledby="thankyouLable" aria-hidden="true">
					    <div className="modal-dialog">
					      <div className="modal-content">
					        <div className="modal-header">
					          <h5 className="modal-title" id="thankyouLable">Thank you for contacting us!</h5>
					          <button type="button" className="close" data-dismiss="modal" aria-label="Close">
					            <span aria-hidden="true">&times;</span>
					          </button>
					        </div>
					        <div className="modal-body">
					          <p className="font-weight-bold">Message Successfully Sent!</p>
					        </div>
					        <div className="modal-footer">
					          <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
					        </div>
					      </div>
					    </div>
					  </div>
					</form>
				</div>

				<div className="col-12 col-lg-6 px-lg-5">
                <Carousel className="border bg-light p-2 mb-5">
                        <Carousel.Item interval={2000}>
                            <img
                            className="d-block w-100"
                            src={img1}
                            />
                        </Carousel.Item>
                        <Carousel.Item interval={2000}>
                            <img
                            className="d-block w-100"
                            src={img2}
                            />
                        </Carousel.Item>
                        <Carousel.Item interval={2000}>
                            <img
                            className="d-block w-100"
                            src={img3}
                            />
                        </Carousel.Item>
                    </Carousel>
					
					<div className="row">
							<div className="col-4 col-lg-4 text-center">
								<button type="button" className="btn btn-lg btn-outline-primary rounded-circle mb-2" disabled>
									<i className="fs-4 bi bi-geo-alt-fill"></i>
								</button>
								<p className="text-break">Brgy. Dolores, Tarlac City, Tarlac, 2300</p>
							</div>

							<div className="col-4 col-lg-4 text-center">
								<button type="button" className="btn btn-lg btn-outline-primary rounded-circle  mb-2" disabled>
									<i className="bi bi-telephone-fill"></i>
								</button>
								<p className="mb-0  text-break">+63 926 0878 583</p>
								<p className="mb-0 text-break">(Mon-Fri, 8:00AM - 5:00PM)</p>
							</div>

							<div className="col-4 col-lg-4 text-center">
								<button type="button" className="btn btn-lg btn-outline-primary rounded-circle  mb-2" disabled>
									<i className="bi bi-envelope-paper-fill"></i>
								</button>
								<p className="text-break">quiambaoangelitot@gmail.com</p>
							</div>
					</div>
				</div>
			</section>
    )
}