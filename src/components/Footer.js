export default function Footer(){
    return(
        <div className="bg-dark text-light text-center d-flex justify-content-center align-items-center">
        <p className="m-2"><span className="m-2 font-weight-bold">The Drip Shop</span>&#169; 2022</p>
        </div>
    )
}