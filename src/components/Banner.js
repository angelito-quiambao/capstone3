export default function Banner(){
    return(
        <section className="row bg-dark">
            <div className="col-12 jumbotron text-center text-light parallax rounded-0 mb-0">
                <div className="border pt-4 pb-5 px-5">
                    <h1 className="display-1 font-weight-bold text-start m-0">The</h1>
                    <h1 className="display-1 font-weight-bold text-start m-0">Drip</h1>
                    <h1 className="display-1 font-weight-bold text-start m-0">Shop</h1>
                </div>
            </div>
        </section>
    )
}