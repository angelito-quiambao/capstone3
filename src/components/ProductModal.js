import { Button, Row, Col, Image, InputGroup, FormControl } from "react-bootstrap";
import { useState, useEffect, useContext } from "react"
import prodIcon from "./../images/prod_icon.jpg"
import CartContext from "../context/CartContext"
const id = localStorage.getItem('id')

export default function ProductModal({prodProps}){

    const [prodName, setProdName] = useState("")
    const [description, setDescription] = useState("")
    const [id, setID] = useState("")
    const [price, setPrice] = useState(0)
    const [stock, setStock] = useState(1)
    const [quantity, setQuantity] = useState(1)
    const [isDisabled, setIsDisabled] = useState(true)

    const { dispatchCart } = useContext(CartContext)

    useEffect(() => {
        const token = localStorage.getItem('token')
        fetch(`https://guarded-escarpment-15423.herokuapp.com/api/products/${prodProps}`, {
        method: "GET",
        headers:{
            "Authorization": `Bearer ${token}`
        }
        })
        .then(result => result.json())
        .then(result => {
            // console.log(result)
            setProdName(result.productName)
            setID(result._id)
            setDescription(result.description)
            setPrice(result.price)
            setStock(result.quantity)
            setIsDisabled(false)
        })
    }, [])

    useEffect(() => {
        if(quantity > stock){
            alert("Product Stock Limit Reach!")
            setQuantity(stock)
        }
        else if(quantity < 1){
            alert("Order Quantity cannot be zero!")
            setQuantity(1)
        }
        else{
            setQuantity(quantity)
        }

    }, [quantity])


    const handleAdd = () =>{
        if(quantity <= stock){
             setQuantity(parseInt(quantity) + 1)
        }

    }

    const handleMinus = () =>{
        if(quantity >= 1){
            setQuantity(parseInt(quantity) - 1)
       }
    }

    // const addItems = () =>{
    //     // console.log("hello")
    //     fetch(`https://guarded-escarpment-15423.herokuapp.com/api/users/order-exist`,{
    //         method: "GET",
    //         headers:{
    //             "Authorization": `Bearer ${token}`
    //         }
    //     })
    //     .then(result => result.json())
    //     .then(result => {
    //         if(result){
    //             // console.log(true)
    //             fetch(`https://guarded-escarpment-15423.herokuapp.com/api/users/add-products`,{
    //                 method: "PATCH",
    //                 headers:{
    //                     "Content-Type": "application/json",
    //                     "Authorization": `Bearer ${token}`
    //                 },
    //                 body: JSON.stringify({
    //                     productId: prodProps,
    //                     quantity: quantity
    //                 })
    //             })
    //             .then(result => result.json())
    //             .then(result => {
    //                 if(result){
    //                     alert('Product added to your cart!')
    //                 }
    //             })
    //         }
    //         else{
    //             // console.log(false)
    //             fetch(`https://guarded-escarpment-15423.herokuapp.com/api/users/checkout`,{
    //                 method: "POST",
    //                 headers:{
    //                     "Content-Type": "application/json",
    //                     "Authorization": `Bearer ${token}`
    //                 },
    //                 body: JSON.stringify({
    //                     productId: prodProps,
    //                     quantity: quantity
    //                 })
    //             })
    //             .then(result => result.json())
    //             .then(result => {
    //                 if(result){
    //                     alert('Product added to your cart!')
    //                 }
    //             })
    //         }
    //     })
    // }

    const addToCart = () =>{
        const addedItem ={
            id: id,
            productName: prodName,
            description: description,
            price: price,
            quantity: quantity,
            stock: stock
        }
        // console.log(addedItem)
        dispatchCart({ type: "ADD", payload: addedItem })
    }

    return (
        <Row>
            <Col xs={12} md={6}>
                <Image src={prodIcon} rounded fluid/>
            </Col>
            <Col xs={12} md={6} className="my-auto">
                <p className="text-left font-weight-bold">Description:</p>
                <p className="text-left">
                    {description}
                </p>
                <p className="text-left"><span className="text-left font-weight-bold">Available Stock:</span> {stock}</p>
                <p className="text-left"><span className="text-left font-weight-bold">Price:</span> &#8369; {price}.00</p>

                <Col xs={12} md={5} className="p-0">
                    <p className="text-left font-weight-bold">Quantity:</p>
                    <InputGroup >
                        <InputGroup.Prepend>
                            <Button variant="outline-secondary" onClick={() => handleMinus()}>-</Button>
                        </InputGroup.Prepend>
                        <FormControl aria-label="Small" type="number" aria-describedby="inputGroup-sizing-sm" min="1" max={stock} className="text-center" value={quantity} onChange={(e) => setQuantity(e.target.value)}/>
                        <InputGroup.Append>
                            <Button variant="outline-secondary" onClick={() => handleAdd()}>+</Button>
                        </InputGroup.Append>
                    </InputGroup>

                    
                    <Button variant="outline-secondary" block className="mt-3" disabled={isDisabled} onClick={() => addToCart()}>Add to Cart</Button>
                </Col>
                
            </Col>
        </Row>
    )
    
}
