export default function Quotes(){

    return(
        <section className="row parallaxv2">
            <div className="col-12 jumbotron text-center text-light rounded-0 mb-0 parallaxv2">
                <div className="border-top border-bottom pt-4 pb-5 px-5">
                    <h1 className="display-4 font-weight-bold text-start m-0">Where have you BEAN</h1>
                    <h1 className="display-1 font-weight-bold text-start m-0">ALL MY LIFE?</h1>
                </div>
            </div>

            <div className="col-12 text-light text-right pb-3">
                <i className="bi bi-cup font-weight-bolder mr-1"></i>
                <span className="font-weight-bolder d-none d-xl-inline">The Drip Shop</span>
            </div>
        </section>
    )
}