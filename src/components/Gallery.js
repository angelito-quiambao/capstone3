import { Carousel, Image } from "react-bootstrap";
import img1 from "./../images/img1.jpg"
import img2 from "./../images/img2.jpg"
import img3 from "./../images/img3.jpg"
export default function Gallery(){
    
    return(
        <section className="row bg-dark secHeight">
                <div className="col-12 col-xl-6 my-auto px-5">
                    <Carousel className="border bg-light p-2">
                        <Carousel.Item interval={2000}>
                            <img
                            className="d-block w-100"
                            src={img1}
                            />
                        </Carousel.Item>
                        <Carousel.Item interval={2000}>
                            <img
                            className="d-block w-100"
                            src={img2}
                            />
                        </Carousel.Item>
                        <Carousel.Item interval={2000}>
                            <img
                            className="d-block w-100"
                            src={img3}
                            />
                        </Carousel.Item>
                    </Carousel>
                </div>

                <div className="col-12 col-xl-6 my-auto px-5 text-light">
                    <h1 className="pb-2 display-4 border-bottom d-inline-block">About Us</h1>
                    <p className="my-4 pr-md-5 text-justify">Where good things come together. <span className="font-weight-bold">The Drip Shop</span> is committed to offering the best coffee available to be enjoyed by our customer!</p>

                    <p className="my-4 pr-md-5 text-justify">Our coffee beans are roasted twice per week and we receive them the next day to ensure their freshness for both the beverages we make in house and the retail beans we offer for sale.</p>

                    <p className="my-4 pr-md-5 text-justify">We invite you to stop by today and to taste it for yourself!</p>
                </div>
        </section>
    )

}