import { Fragment, useEffect, useState } from "react";
import { Table, Container, Button, Modal } from "react-bootstrap";

export default function UserDashboard(){

    const [users, setUsers] = useState([])

    const fetchData = () =>{
        fetch(`https://guarded-escarpment-15423.herokuapp.com/api/users`, {
            method: "GET",
            headers:{
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(result => result.json())
        .then(result => {
            setUsers(
                result.map(user => {
                    return (
                        <tr key={user._id}>
                            <td className="col-2">{user.firstName}</td>
                            <td className="col-2">{user.lastName}</td>
                            <td className="col-3">{user.email}</td>
                            <td className="col-2">{user.isAdmin ? "Admin" : "User"}</td>
                            <td className="col-3">
                                {
                                    user.isAdmin
                                    ?
                                    <Button className="btn btn-danger btn-sm btn-block" >Set As User</Button>
                                    :
                                    <Button className="btn btn-success btn-sm btn-block" >Set As Admin</Button>
                                }
                            </td>
                        </tr>
                    )
                })
            )

        })
   
    }

    useEffect(() =>{
        fetchData()
    }, [])

    return(
        <Container className="Container">
            <h1 className="my-3 text-center">Current Users</h1>
            <Table>
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    { users }
                </tbody>
            </Table>
        </Container> 
    )
}