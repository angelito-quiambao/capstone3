import { useState, useEffect } from 'react'
import { Container, Row, Col, Form, Button } from 'react-bootstrap'
import { useNavigate } from 'react-router-dom'
const token = localStorage.getItem('token')

export default function UpdateProduct({prodProps}){

    const [productName, setProductName] = useState("")
    const [description, setDescription] = useState("")
    const [price, setPrice] = useState(0)
    const [quantity, setQuantity] = useState(0)
    const [isDisabled, setIsDisabled] = useState(true)

    const fetchProd = () => {
        fetch(`https://guarded-escarpment-15423.herokuapp.com/api/products/${prodProps}`, {
        method: "GET",
        headers:{
            "Authorization": `Bearer ${token}`
        }
        })
        .then(result => result.json())
        .then(result => {
            setProductName(result.productName)
            setDescription(result.description)
            setPrice(result.price)
            setQuantity(result.quantity)
        })
    }

    useEffect(() => {
        fetchProd()
    }, [])

    useEffect(() => {

        if(productName === "" || description === "" || price === 0 || quantity === 0){
            setIsDisabled(true)
        }
        else{
            setIsDisabled(false)
        }

    }, [productName, description, price, quantity])

    const updateProduct = (e) => {
        e.preventDefault()

		fetch(`https://guarded-escarpment-15423.herokuapp.com/api/products/${prodProps}`,{
			method: "PUT",
			headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
				productName: productName,
				description: description,
				price: price,
                quantity: quantity
			})
		})
		.then(result => result.json())
		.then(result => {
			console.log(result)
			if(result){

				alert('Product successfully updated!')
			}
		})

    }

    return (
        <Container className="container my-5">
			<Form onSubmit={(e) => updateProduct(e)}>
				<Row>
					<Col xs={12} md={6}>
						<Form.Group className="mb-3">
					    	<Form.Control
					    		placeholder="Product Name"
					    		type="text" 
					    		 value={productName}
                                 onChange={(e) => setProductName(e.target.value)}
					    	/>
						</Form.Group>
					</Col>
					<Col xs={6}  md={3}>
						<Form.Group className="mb-3">
					    	<Form.Control
                                placeholder="Price"
					    		type="number" 
                                value={price}
                                 onChange={(e) => setPrice(e.target.value)}
					    	/>
						</Form.Group>
					</Col>
                    <Col xs={6}  md={3}>
						<Form.Group className="mb-3">
					    	<Form.Control
                                placeholder="Qty."
					    		type="number" 
					    		value={quantity}
                                 onChange={(e) => setQuantity(e.target.value)}
					    	/>
						</Form.Group>
					</Col>
				</Row>
				<Row>
					<Col xs={12}  md={12}>
						<Form.Group className="mb-3">
					    	<Form.Control
					    		placeholder="Course Description"
					    		type="text" 
					    		value={description}
                                 onChange={(e) => setDescription(e.target.value)}
					    	/>
						</Form.Group>
					</Col>
				</Row>
				<Button className="btn btn-dark btn-block" disabled={isDisabled} type="submit">Save</Button>
			</Form>
		</Container>
    )
}