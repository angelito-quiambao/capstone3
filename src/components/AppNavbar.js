import {Fragment, useContext, useEffect, useState } from "react"
import {Navbar, Nav, Container, Badge} from "react-bootstrap"
import { useLocation, Link } from "react-router-dom"
import UserContext from "../context/UserContext"
import AdminContext from "../context/AdminContext"
import CartContext from "../context/CartContext"

const token = localStorage.getItem('token')
const isAdmin = localStorage.getItem('isAdmin')

export default function AppNavbar(){

    const {state, dispatch} = useContext(UserContext)
    const {state2, dispatch2} = useContext(AdminContext)
    const { stateCart, dispatchCart } = useContext(CartContext)

    // console.log(stateCart)
    const location = useLocation()


    useEffect(() => {
        if(token){
            dispatch({type: "USER", payload: true})
        }
        else{
            dispatch({type: "USER", payload: null})
        }

        if(isAdmin === "true"){
            dispatch2({type: "isAdmin", payload: true})
        }
        else{
            dispatch2({type: "isAdmin", payload: false})
        }
    }, [])


    const NavLinks2 = () => {
        if(state2){
            // console.log(true)
            return(
            <Link to="/adminView" className="text-light nav-link" >Admin Panel</Link>
            )
        }
        else{
            // console.log(false)
            return(
            <Fragment>
                <Link to="/cart" className="text-light nav-link" ><i className="bi bi-cart-fill"></i><Badge>{stateCart.length === 0 ? "" : `(${stateCart.length})`}</Badge></Link>
                <Link to="/products" className="text-light nav-link">Products</Link>
            </Fragment>
            )
        }
    }

    const NavLinks = () =>{

        if(state === true){
            return (
                <Fragment>
                    <NavLinks2 />
                    <Link to="/logout" className="text-light nav-link" >Logout</Link>
                </Fragment>
            )
        }
        else{
            return(
                <Fragment>
                    <Link to="/login" className="text-light nav-link" >Login</Link>
                    <Link to="/register" className="text-light nav-link" >Register</Link>
                </Fragment>
            )
        }
    }

    return (
        <Navbar className ="mx-auto" bg={location.pathname === "/" || location.pathname === "/login" ||location.pathname === "/register" ? "transparent" : "dark"} expand="lg" fixed={location.pathname === "/" || location.pathname === "/login" ||location.pathname === "/register" ? "top" : "none"}>
            <Link to="/" className="text-light navbar-brand">
                <i className="bi bi-cup font-weight-bolder mr-1"></i>
                <span className="font-weight-bolder d-none d-xl-inline">The Drip Shop</span> 
            </Link>
            <Container className="m-0 ml-auto">
                <Navbar.Toggle aria-controls="basic-navbar-nav text" />
                <Navbar.Collapse id="basic-navbar-nav" className="text-light">
                    <Nav className="ml-auto">
                        <NavLinks />   
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}