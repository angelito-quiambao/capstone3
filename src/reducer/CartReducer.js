export const CartReducer = (state, action) =>{

    switch(action.type){
        case "ADD":
            const item = state.find((product) => product.id === action.payload.id)

            if(item){
                return ( 
                    state.map((product) => {
                        if(product.id === action.payload.id) {
                            return({
                                    ...item,
                                     quantity: item.quantity += action.payload.quantity
                                    
                                })
                        }
                        else{
                            return product
                        }
                    })
                )
            }
            else{
                return [...state, action.payload]
        }

        case "INC":
            const inc = state.find((product) => product.id === action.payload)
            // console.log(inc)

            if(inc){
                return ( 
                    state.map((product) => {
                        if(product.id === action.payload) {
                            // console.log(inc.quantity + 1)
                            return({
                                    ...inc,
                                     quantity: inc.quantity + 1
                                    
                                })
                        }
                        else{
                            return product
                        }
                    })
                )

            }

            case "DEC":
                const dec = state.find((product) => product.id === action.payload)
                // console.log(dec)
                if(dec){
                    return ( 
                        state.map((product) => {
                            if(product.id === action.payload) {
                                // console.log(dec.quantity - 1)
                                return({
                                        ...dec,
                                         quantity: dec.quantity - 1
                                        
                                    })
                            }
                            else{
                                return product
                            }
                        })
                    )
    
                }
                
        case "DELETE":
            const itemIndex = state.findIndex((product) => product.id === action.payload)
            const newState = [...state];
            newState.splice(itemIndex, 1);
            return newState;

        case "CLEAR":
            return action.payload

        default:
            throw new Error(`unknown action ${action.type}`);
    }
    
}