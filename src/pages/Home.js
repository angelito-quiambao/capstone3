import { Fragment } from "react";
import Banner from "../components/Banner";
import Contact from "../components/Contact";
import Gallery from "../components/Gallery";
import Quotes from "../components/Quote";

export default function Home(){
    
    return(
        <Fragment>
            <main className="container-fluid">
                <Banner/>
                <Gallery/>
                <Quotes />
                <Contact />
            </main>
        </Fragment>
    )
}