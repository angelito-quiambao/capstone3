import { Fragment, useEffect, useState } from "react";
import { Table, Container, Button, Modal } from "react-bootstrap";
import { Link } from "react-router-dom";
import AddProduct from "../components/AddProduct";
import UpdateProduct from "../components/UpdateProduct";
import UserDashboard from "../components/UserDashboard";

export default function AdminView(){

    const [allProduct, setAllProducts] = useState([])
    const [addProd, setAddProd] = useState(false);
    const [userDash, setUserDash] = useState(false);
    const [updateProd, setUpdateProd] = useState(false);
    const [productId, setProductId] = useState(0)


    const fetchData = () =>{
        fetch(`https://guarded-escarpment-15423.herokuapp.com/api/products`, {
            method: "GET",
            headers:{
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(result => result.json())
        .then(result => {
            setAllProducts(result.map(product => {
            return(
                <tr key={product._id}>
                <td className="col-3">{product.productName}</td>
                <td className="col-3">{product.description}</td>
                <td className="col-2">{product.price}</td>
                <td className="col-2">{product.isActive ? "Available" : "Unavailable"}</td>
                <td className="col-2">
                    {
                        product.isActive
                        ?
                        <Fragment>
                            <Button className="btn btn-danger btn-sm mx-2" onClick={ () => handleDisable(product._id) }>Disable</Button>
                            <Button className="btn btn-primary btn-sm mx-2" onClick={() => showUpdateModal(product._id)}>Update</Button>
                        </Fragment>
                        :
                        <Fragment>
                            <Button className="btn btn-success btn-sm mx-2" onClick={ () => handleEnable(product._id) }>Enable</Button>
                            <Button className="btn btn-secondary btn-sm mx-2">Delete</Button>
                        </Fragment>
                    }
                </td>
            </tr>
            )
        }))
      })
    }

    const handleDisable = (prodId) =>{
        console.log(prodId)

        fetch(`https://guarded-escarpment-15423.herokuapp.com/api/products/${prodId}/archive`, {
            method: "PATCH",
            headers:{
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.json())
        .then(response => {
            if(response){
                fetchData()
            }
        })
    }

    const handleEnable = (prodId) =>{
        console.log(prodId)

        fetch(`https://guarded-escarpment-15423.herokuapp.com/api/products/${prodId}/unarchive`, {
            method: "PATCH",
            headers:{
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.json())
        .then(response => {
            if(response){
                fetchData()
            }
        })
    }
    const handleClose = () => {
        setUpdateProd(false)
        setAddProd(false)
        fetchData()
    };

    const closeUserModal = () =>{
        setUserDash(false)
    }

    const showUpdateModal = (prodId) => {
        setUpdateProd(true)
        setProductId(prodId)
    };

    const showAddModal = () => {
        setAddProd(true)
    };

    const showUserModal = () => {
        setUserDash(true)
    };

    console.log(productId)

    useEffect(() => {
        fetchData()
    }, [])


    return (
        <Container className="Container secHeight">
            <h1 className="my-5 text-center">Product Dashboard</h1>
            <div className="text-right">
                <Button className="btn btn-info  m-2" onClick={() => showAddModal()}>Add New Product</Button>
                <Button className="btn btn-info  m-2" onClick={() => showUserModal()}>User Dashboard</Button>
            </div>

            <Table>
                <thead>
                    <tr>
                        <th>Product Name</th>
                        <th>Description</th>
                        <th>Price</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    { allProduct }
                </tbody>
            </Table>

            <Modal show={updateProd} onHide={handleClose} size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
            <Modal.Header closeButton>
            <Modal.Title>Update a Product</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <UpdateProduct prodProps={productId}/>
            </Modal.Body>
            </Modal>

            <Modal show={addProd} onHide={handleClose} size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
            <Modal.Header closeButton>
            <Modal.Title>Add New Product</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <AddProduct />
            </Modal.Body>
            </Modal>

            <Modal show={userDash} onHide={closeUserModal} size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
            <Modal.Header closeButton>
            <Modal.Title>Set User Authority</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <UserDashboard />
            </Modal.Body>
            </Modal>
        </Container>
    )
}