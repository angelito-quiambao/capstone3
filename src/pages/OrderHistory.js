import { useEffect, useState, useContext } from "react";
import CartContext from "../context/CartContext"
import { Link } from "react-router-dom"
import { Table, Container, Button, InputGroup, FormControl } from "react-bootstrap"

export default function OrderHistory(){

    const [amount, setAmount] = useState(0)
    const [orders, setOrders] = useState([])

    const { stateCart, dispatchCart } = useContext(CartContext)

    console.log(stateCart)

    useEffect(() => {
        // console.log('render')
        setAmount(stateCart.reduce((amount, item) => amount + (item.price*item.quantity), 0))

        setOrders(stateCart.map( item=> {
            const subtotal = item.price * item.quantity
            return(
                <tr key={item.id}>
                    <td className="col-2">{item.productName}</td>
                    <td className="col-4">{item.description}</td>
                    <td className="col-1 text-center">{item.price}</td>
                    <td className="col-1 text-center">{item.quantity}</td>
                    <td className="col-1 text-center">{subtotal}</td>
                </tr>
            )
        }))
    }, [])

    const handleClear = () =>{
        // console.log("clear")
        dispatchCart({type: "CLEAR", payload: []})
    }

    return (
        <Container className="Container secHeight">
                <h1 className="my-5 text-center">Thank you for your purchase!</h1>
    
                <Table>
                <thead>
                    <tr>
                        <th>Product Name</th>
                        <th>Description</th>
                        <th className="text-center">Price</th>
                        <th className="text-center">Quantity</th>
                        <th className="text-center">Subtotal</th>
                    </tr>
                </thead>
                <tbody>
                    {orders}
                </tbody>
                <tfoot>
                    <tr>
                        <th className="text-right" colSpan="4" scope="row">Total Amount:</th>
                        <th>&#8369; <span></span>{amount}.00</th>
                    </tr>
                </tfoot>
            </Table>

            <Link to={'/products'} className="btn btn-outline-secondary btn-lg btn-block" onClick={() => handleClear()}>Shop Again</Link>
        </Container>
    )
}