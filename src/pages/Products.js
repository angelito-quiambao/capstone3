import ProductCard from "../components/ProductCard"
import {useState, useEffect, Fragment} from "react";
import { Container, Row } from "react-bootstrap";


export default function Products(){

    const [productList, setProducList] = useState([])

    useEffect( () => {
        const token = localStorage.getItem('token')

            fetch(`https://guarded-escarpment-15423.herokuapp.com/api/products`, {
                method: "GET",
                headers:{
                    "Authorization": `Bearer ${token}`
                }
                })
                .then(result => result.json())
                .then(result => {
                    console.log(result)
                    setProducList(result.map(product => {
                        return <ProductCard key={product._id} productProp={product} />
                        })
                    ) 
            })
    }, [])  

    return(
        <Fragment>
            <Container className="container secHeight mb-3">
            <h1 className="font-weight-bold my-5 text-center text-uppercase">Available Coffee Beans</h1>
                <Row className="row">
                    {productList}
                </Row>
            </Container>
        </Fragment>
    )
}