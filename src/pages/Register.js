import {useState, useEffect} from "react";
import {Form, Button} from "react-bootstrap"
import {useNavigate, Link} from 'react-router-dom'

export default function Register(){
    const navigate = useNavigate();

    const [firstName, setFN] = useState("")
    const [lastName, setLN] = useState("")
    const [email, setEmail] = useState("")
    const [password, setPW] = useState("")
    const [vPassword, setVPW] = useState("")
    const [isDisabled, setIsDisabled] = useState(true)

    useEffect(() => {
        if(firstName === "" || lastName === "" || email === "" || password === "" || vPassword === ""){
            setIsDisabled(true)
        }
        else{
            if(password !== vPassword){
                setIsDisabled(true)
            }
            else{
                setIsDisabled(false)
            }
        }

    }, [firstName, lastName, email, password, vPassword])

    const registerUser = (e) =>{
        e.preventDefault()
        // console.log("Hello")

        fetch(`https://guarded-escarpment-15423.herokuapp.com/api/users/email-exists`, {
             method: "POST",
             headers:{
                "Content-Type": "application/json"
             },
             body: JSON.stringify({
                "email": email,
             })
         })
         .then(result => result.json())
         .then(result =>{
             if(result === false){
                 fetch(`https://guarded-escarpment-15423.herokuapp.com/api/users/register`, {
                    method: "POST",
                    headers:{
                       "Content-Type": "application/json"
                    },
                    body: JSON.stringify({
                       "firstName": firstName,
                       "lastName": lastName,
                       "email": email,
                       "password": password
                    })
                 })

                .then(result => result.json())
				.then(result =>{
					if(result){
						alert(`User successfully registered!`)
						navigate('/login')
					}
					else{
						alert(`Please try again`)
					}
				})
             }
             else{
                alert("Email already Exist!")
             }
         })
    }

    return(
        <div className="form" id="bg_shop">
            <div className="container">
              <div className="row d-flex justify-content-center align-items-center">
                <div className="col-12 col-md-9 col-lg-7 col-xl-6">
                  <div className="card rounded-lg">
                    <div className="card-body p-5">
                      <h2 className="text-uppercase text-center mb-5">Create an account</h2>

                    <Form onSubmit={(e) => registerUser(e)}>
                        <Form.Group className="mb-3">
                            <Form.Label>First Name</Form.Label>
                            <Form.Control type="text" value={firstName} onChange={(e) => setFN(e.target.value)}/>
                        </Form.Group>

                        <Form.Group className="mb-3">
                            <Form.Label>Last Name</Form.Label>
                            <Form.Control type="text" value={lastName} onChange={(e) => setLN(e.target.value)}/>
                        </Form.Group>

                        <Form.Group className="mb-3">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type="email" value={email} onChange={(e) => setEmail(e.target.value)}/>
                        </Form.Group>

                        <Form.Group className="mb-3">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" value={password} onChange={(e) => setPW(e.target.value)}/>
                        </Form.Group>

                        <Form.Group className="mb-3">
                            <Form.Label>Verify Password</Form.Label>
                            <Form.Control type="password" value={vPassword} onChange={(e) => setVPW(e.target.value)}/>
                        </Form.Group>

                        <Button variant="outline-dark"  type="submit" disabled={isDisabled} block className="mb-5 mt-4">Submit</Button>

                        <p className="text-center text-muted">Have already an account? <Link to="/login" className="fw-bold text-body"><u>Login here</u></Link></p>

                    </Form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
    )
}