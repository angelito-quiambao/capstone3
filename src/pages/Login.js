import {useState, useEffect, useContext} from "react";
import {Form, Button } from "react-bootstrap"
import {useNavigate, Link} from 'react-router-dom'
import UserContext from "../context/UserContext"
import AdminContext from "../context/AdminContext"


export default function Login(){


    const [email, setEmail] = useState("")
    const [password, setPW] = useState("")
    const [isDisabled, setIsDisabled] = useState(true)

    const { dispatch } = useContext(UserContext)
    const { dispatch2 } = useContext(AdminContext)
    const navigate = useNavigate();

    useEffect(() => {
        if( email === "" || password === ""){
            setIsDisabled(true)
        }
        else{
            setIsDisabled(false)
        }

    }, [email, password])

    const loginUser = (e) =>{
        e.preventDefault()

        fetch(`
        https://guarded-escarpment-15423.herokuapp.com/api/users/login`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })

    //server response
        .then(result => result.json())
        .then(result => {

            // console.log(result)
            if(result){
                localStorage.setItem('token', result.token)
                const  token = localStorage.getItem('token')

                fetch(`https://guarded-escarpment-15423.herokuapp.com/api/users/profile`, {
                    method: "GET",
                    headers:{
                        "Authorization": `Bearer ${token}`
                    }
                })
                .then(result => result.json())
                .then(result => {
                    alert('Login successfully!')
                    
                    localStorage.setItem('isAdmin', result.isAdmin)
                    localStorage.setItem('id', result._id)

                    if(localStorage.getItem('isAdmin') === "true"){
                        dispatch2({type: "isAdmin", payload: true})
                    }
                    
                    dispatch({type: "USER", payload: true})
                })

                setEmail("")
				setPW("")

                navigate('/')

            }
            else{
                alert('Please check your email and password!')
            }
        })
    }


    return(
        <div className="form" id="bg_shop">
            <div className="container">
              <div className="row d-flex justify-content-center align-items-center">
                <div className="col-12 col-md-9 col-lg-7 col-xl-6">
                  <div className="card rounded-lg">
                    <div className="card-body p-5">
                      <h2 className="text-uppercase text-center mb-5">Login Your Account</h2>
                        
                      <Form onSubmit={(e) => loginUser(e)}>

                          <Form.Group className="mb-3">
                              <Form.Label>Email address</Form.Label>
                              <Form.Control type="email" value={email} onChange={(e) => setEmail(e.target.value)}/>
                          </Form.Group>

                          <Form.Group className="mb-3">
                              <Form.Label>Password</Form.Label>
                              <Form.Control type="password" value={password} onChange={(e) => setPW(e.target.value)}/>
                          </Form.Group>

                          <Button variant="outline-dark"  type="submit" disabled={isDisabled} block className="mb-5 mt-4">Login</Button>

                          <p className="text-center text-muted">Don't have an account? <Link to="/register" className="fw-bold text-body"><u>Create Account Here.</u></Link></p>
                      </Form>
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
    )

}