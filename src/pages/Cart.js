import { Table, Container, Button, InputGroup, FormControl } from "react-bootstrap"
import { useEffect, useState, useContext } from "react";
import CartContext from "../context/CartContext"
import { Link } from "react-router-dom"
const token = localStorage.getItem('token')


export default function Cart(){

    const [amount, setAmount] = useState(0)
    const [orders, setOrders] = useState([])
    const [trigger, setTrigger] = useState(0)

    const { stateCart, dispatchCart } = useContext(CartContext)

    useEffect(() => {
        // console.log('render')
        setAmount(stateCart.reduce((amount, item) => amount + (item.price*item.quantity), 0))

        setOrders(stateCart.map( item=> {
            const subtotal = item.price * item.quantity
            return(
                <tr key={item.id}>
                    <td>{item.productName}</td>
                    <td>{item.price}</td>
                    <td>
                    <InputGroup >
                        <InputGroup.Prepend>
                            <Button variant="outline-secondary" onClick ={() => handleMinus(item.id, item.quantity, item.price)}>-</Button>
                        </InputGroup.Prepend>
                        <FormControl aria-label="Small" type="number" aria-describedby="inputGroup-sizing-sm" min="1" max={item.stock} className="text-center" value={item.quantity} readOnly />
                        <InputGroup.Append>
                            <Button variant="outline-secondary" onClick ={() => handleAdd(item.id, item.quantity, item.price)}>+</Button>
                        </InputGroup.Append>
                    </InputGroup>
                    </td>
                    <td>{subtotal}</td>
                    <td><Button variant="outline-dark" size="sm" onClick ={() => handleRemove(item.id)}>Remove</Button></td>
                </tr>
            )
        }))
    }, [trigger])


    const handleRemove = (id) =>{
        dispatchCart({ type: "DELETE", payload: id })
        setTrigger(trigger+1)
    }
    
    const handleAdd = (id, price) =>{
        dispatchCart({ type: "INC", payload: id })
        setTrigger(trigger+1)
    }
    // console.log(trigger)

    const handleMinus = (id, qty) =>{
        if(qty <= 1){
            dispatchCart({ type: "DELETE", payload: id })
        }
        else{
            dispatchCart({ type: "DEC", payload: id })
        }
        setAmount(stateCart.reduce((amount, item) => amount + item.price, 0))
        setTrigger(trigger+1)
    }

    if(stateCart.length === 0){
        return(
            <Container className="Container secHeight">
                <h1 className="my-5 text-center">Order Summary</h1>
    
                <Table>
                <thead>
                    <tr>
                        <th>Product Name</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Subtotal</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th colSpan="6">
                            <h2 className="my-5 text-center" >Your Cart is Empty!</h2>
                        </th>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <th className="text-right" colSpan="4" scope="row">Total Amount:</th>
                        <th>&#8369; <span></span>0.00</th>
                    </tr>
                </tfoot>
            </Table>
        </Container>
        ) 
        
    }
    else{
        return (
            <Container className="Container secHeight">
                <h1 className="my-5 text-center">Order Summary</h1>
    
                <Table>
                <thead>
                    <tr>
                        <th>Product Name</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Subtotal</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {orders}
                </tbody>
                <tfoot>
                    <tr>
                        <th className="text-right" colSpan="4" scope="row">Total Amount:</th>
                        <th>&#8369; <span></span>{amount}.00</th>
                    </tr>
                </tfoot>
            </Table>

            <Link to={'/checkout'} className="btn btn-outline-secondary btn-lg btn-block">Check Out</Link>
        </Container>
        )
    }
}
