import { useContext, useEffect } from "react"
import UserContext from "../context/UserContext"
import AdminContext from "../context/AdminContext"
import CartContext from "../context/CartContext"
import { Navigate } from "react-router-dom"

export default function Logout(){

    const { dispatch } = useContext(UserContext)
    const { dispatch2 } = useContext(AdminContext)
    const { dispatchCart } = useContext(CartContext)
    
    useEffect(() => {
        localStorage.clear()
        dispatch({type: "USER", payload: null})
        dispatch2({type: "isAdmin", payload: null})
        dispatchCart({type: "CLEAR", payload:[]})
    }, [])

    return (
        <Navigate to="/login" replaced/>
    )
}